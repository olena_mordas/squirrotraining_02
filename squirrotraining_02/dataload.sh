#!/bin/bash

CLUSTER="http://localhost"
TOKEN="46e2fc4359ba774f8bbcb6915b69dec1819a469d8e657e82b94928d3db91d7a2dffb89a6dcc72fdaa781a778ce9f5aa1f635b8fd0f1b5fccc04290b2245cfeff"
PROJECT_ID="8BJVo-Q9Sre2jMj1-_i7Ow"

squirro_data_load  -v \
    --cluster $CLUSTER \
    --project-id $PROJECT_ID \
    --token $TOKEN \
    --source-script medline.py \
    --source-path pubmed \
    --map-id PMID \
    --map-title TI \
    --map-created-at DA \
    --map-body AB \
    --source-name "PubMed" \
    --facets-file facets.json