<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<pre>
PMID- 26785438
OWN - NLM
STAT- Publisher
DA  - 20160119
LR  - 20160119
IS  - 1528-1175 (Electronic)
IS  - 0003-3022 (Linking)
VI  - 124
IP  - 2
DP  - 2016 Feb
TI  - Anesthesia: A Comprehensive Review, Fifth Edition.
PG  - 516
FAU - Colin, Brian
AU  - Colin B
AD  - Duke University Medical Center, Durham, North Carolina (B.C.).
      brian.colin@duke.edu.
FAU - Thompson, Annemarie
AU  - Thompson A
LA  - ENG
PT  - JOURNAL ARTICLE
TA  - Anesthesiology
JT  - Anesthesiology
JID - 1300217
CRDT- 2016/01/20 06:00
AID - 10.1097/ALN.0000000000000971 [doi]
AID - 00000542-201602000-00045 [pii]
PST - ppublish
SO  - Anesthesiology. 2016 Feb;124(2):516.
</pre>