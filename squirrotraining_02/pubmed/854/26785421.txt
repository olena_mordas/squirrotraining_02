<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<pre>
PMID- 26785421
OWN - NLM
STAT- Publisher
DA  - 20160119
LR  - 20160119
IS  - 1536-3686 (Electronic)
IS  - 1075-2765 (Linking)
DP  - 2016 Jan 16
TI  - Successful Treatment With Bosentan for Digital Ulcers Related to Mixed
      Cryoglobulinemia: A Case Report.
AB  - A 49-year-old man diagnosed with genotype 1 hepatitis C, CD5-positive marginal
      zone lymphoma, and mixed cryoglobulinemia type II developed skin ulcers and
      necrosis in his right foot. He was treated with amlodipine, corticosteroids,
      plasmapheresis, alprostadil, rituximab, and cyclophosphamide without a
      satisfactory response. For this reason, he required a partial amputation of the
      second, third, and fifth fingers of the right foot. To prevent ulcer
      deterioration of the first finger, bosentan was initiated. After 10 months of
      treatment, the ulcer completely healed and no adverse effects were experienced by
      the patient.
FAU - de Lorenzo-Pinto, Ana
AU  - de Lorenzo-Pinto A
AD  - Departments of 1Pharmacy and 2Internal Medicine, Hospital General Universitario
      Gregorio Maranon, Madrid, Spain.
FAU - Pinilla Llorente, Blanca
AU  - Pinilla Llorente B
LA  - ENG
PT  - JOURNAL ARTICLE
DEP - 20160116
TA  - Am J Ther
JT  - American journal of therapeutics
JID - 9441347
CRDT- 2016/01/20 06:00
AID - 10.1097/MJT.0000000000000384 [doi]
PST - aheadofprint
SO  - Am J Ther. 2016 Jan 16.
</pre>