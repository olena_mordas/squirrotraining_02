<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<pre>
PMID- 26785481
OWN - NLM
STAT- Publisher
DA  - 20160119
LR  - 20160119
IS  - 1095-9203 (Electronic)
IS  - 0036-8075 (Linking)
VI  - 350
IP  - 6265
DP  - 2015 Dec 4
TI  - Gut microbiota and aging.
PG  - 1214-1215
AB  - The potential for the gut microbiota to affect health has a particular relevance 
      for older individuals. This is because the microbiota may modulate aging-related 
      changes in innate immunity, sarcopaenia, and cognitive function, all of which are
      elements of frailty. Both cell culture-dependent and -independent studies show
      that the gut microbiota of older people differs from that of younger adults.
      There is no chronological threshold or age at which the composition of the
      microbiota suddenly alters; rather, changes occur gradually with time. Our
      detailed analyses have separated the microbiota into groups associated with age, 
      long-term residential care, habitual diet, and degree of retention of a core
      microbiome. We are beginning to understand how these groups change with aging and
      how they relate to clinical phenotypes. These data provide a framework for
      analyzing microbiota-health associations, distinguishing correlation from
      causation, identifying microbiota interaction with physiological aging processes,
      and developing microbiota-based health surveillance for older adults.
CI  - Copyright (c) 2015, American Association for the Advancement of Science.
FAU - O'Toole, Paul W
AU  - O'Toole PW
AD  - School of Microbiology and APC Microbiome Institute, University College Cork,
      Cork T12 Y337, Ireland. pwotoole@ucc.ie.
FAU - Jeffery, Ian B
AU  - Jeffery IB
AD  - School of Microbiology and APC Microbiome Institute, University College Cork,
      Cork T12 Y337, Ireland.
LA  - ENG
PT  - REVIEW
PT  - JOURNAL ARTICLE
TA  - Science
JT  - Science (New York, N.Y.)
JID - 0404511
CRDT- 2016/01/20 06:00
AID - 350/6265/1214 [pii]
AID - 10.1126/science.aac8469 [doi]
PST - ppublish
SO  - Science. 2015 Dec 4;350(6265):1214-1215.
</pre>