<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<pre>
PMID- 26785402
OWN - NLM
STAT- Publisher
DA  - 20160119
LR  - 20160119
IS  - 1539-3704 (Electronic)
IS  - 0003-4819 (Linking)
DP  - 2016 Jan 19
TI  - Appropriate Antibiotic Use for Acute Respiratory Tract Infection in Adults:
      Advice for High-Value Care From the American College of Physicians and the
      Centers for Disease Control and Prevention.
LID - 10.7326/M15-1840 [doi]
AB  - Background: Acute respiratory tract infection (ARTI) is the most common reason
      for antibiotic prescription in adults. Antibiotics are often inappropriately
      prescribed for patients with ARTI. This article presents best practices for
      antibiotic use in healthy adults (those without chronic lung disease or
      immunocompromising conditions) presenting with ARTI. Methods: A narrative
      literature review of evidence about appropriate antibiotic use for ARTI in adults
      was conducted. The most recent clinical guidelines from professional societies
      were complemented by meta-analyses, systematic reviews, and randomized clinical
      trials. To identify evidence-based articles, the Cochrane Library, PubMed,
      MEDLINE, and EMBASE were searched through September 2015 using the following
      Medical Subject Headings terms: "acute bronchitis," "respiratory tract
      infection," "pharyngitis," "rhinosinusitis," and "the common cold." High-Value
      Care Advice 1: Clinicians should not perform testing or initiate antibiotic
      therapy in patients with bronchitis unless pneumonia is suspected. High-Value
      Care Advice 2: Clinicians should test patients with symptoms suggestive of group 
      A streptococcal pharyngitis (for example, persistent fevers, anterior cervical
      adenitis, and tonsillopharyngeal exudates or other appropriate combination of
      symptoms) by rapid antigen detection test and/or culture for group A
      Streptococcus. Clinicians should treat patients with antibiotics only if they
      have confirmed streptococcal pharyngitis. High-Value Care Advice 3: Clinicians
      should reserve antibiotic treatment for acute rhinosinusitis for patients with
      persistent symptoms for more than 10 days, onset of severe symptoms or signs of
      high fever (&gt;39 degrees C) and purulent nasal discharge or facial pain lasting
      for at least 3 consecutive days, or onset of worsening symptoms following a
      typical viral illness that lasted 5 days that was initially improving (double
      sickening). High-Value Care Advice 4: Clinicians should not prescribe antibiotics
      for patients with the common cold.
FAU - Harris, Aaron M
AU  - Harris AM
FAU - Hicks, Lauri A
AU  - Hicks LA
FAU - Qaseem, Amir
AU  - Qaseem A
CN  - High Value Care Task Force of the American College of Physicians and for the
      Centers for Disease Control and Prevention
LA  - ENG
PT  - JOURNAL ARTICLE
DEP - 20160119
TA  - Ann Intern Med
JT  - Annals of internal medicine
JID - 0372351
CRDT- 2016/01/20 06:00
AID - 2481815 [pii]
AID - 10.7326/M15-1840 [doi]
PST - aheadofprint
SO  - Ann Intern Med. 2016 Jan 19. doi: 10.7326/M15-1840.
</pre>