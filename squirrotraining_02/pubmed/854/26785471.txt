<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<pre>
PMID- 26785471
OWN - NLM
STAT- Publisher
DA  - 20160119
LR  - 20160119
IS  - 1095-9203 (Electronic)
IS  - 0036-8075 (Linking)
VI  - 350
IP  - 6265
DP  - 2015 Dec 4
TI  - Response to Comment on "Worldwide evidence of a unimodal relationship between
      productivity and plant species richness".
PG  - 1177
AB  - Laanisto and Hutchings claim that the local species pool is a more important
      predictor of local plant species richness than biomass and that when the species 
      pool is considered, there is no hump-backed relationship between biomass and
      richness. However, we show that by calculating a more appropriate measure of
      species pool, community completeness, both regional and local processes shape
      local richness.
CI  - Copyright (c) 2015, American Association for the Advancement of Science.
FAU - Fraser, Lauchlan H
AU  - Fraser LH
AD  - Department of Natural Resource Sciences, Thompson Rivers University, Kamloops,
      BC, Canada. lfraser@tru.ca.
FAU - Partel, Meelis
AU  - Partel M
AD  - Department of Botany, Institute of Ecology and Earth Sciences, University of
      Tartu, Tartu, Estonia.
FAU - Pither, Jason
AU  - Pither J
AD  - Department of Biology, University of British Columbia, Okanagan Campus, Kelowna, 
      BC, Canada.
FAU - Jentsch, Anke
AU  - Jentsch A
AD  - Department of Disturbance Ecology, Bayreuth Center of Ecology and Environmental
      Research, University of Bayreuth, Bayreuth, Germany.
FAU - Sternberg, Marcelo
AU  - Sternberg M
AD  - Department of Molecular Biology and Ecology of Plants, Tel Aviv University,
      Tel-Aviv, Israel.
FAU - Zobel, Martin
AU  - Zobel M
AD  - Department of Botany, Institute of Ecology and Earth Sciences, University of
      Tartu, Tartu, Estonia.
LA  - ENG
PT  - JOURNAL ARTICLE
TA  - Science
JT  - Science (New York, N.Y.)
JID - 0404511
CRDT- 2016/01/20 06:00
AID - 350/6265/1177-c [pii]
AID - 10.1126/science.aad4874 [doi]
PST - ppublish
SO  - Science. 2015 Dec 4;350(6265):1177.
</pre>