<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<pre>
PMID- 26785426
OWN - NLM
STAT- Publisher
DA  - 20160119
LR  - 20160119
IS  - 1364-548X (Electronic)
IS  - 1359-7345 (Linking)
DP  - 2016 Jan 19
TI  - Enhanced performance in gas adsorption and Li ion batteries by docking Li in a
      crown ether-based metal-organic framework.
AB  - Incorporating supramolecular interaction units, crown ether rings, into
      metal-organic frameworks enables the docking of metal ions through complexation
      for enhanced performance in H2 and CO2 adsorption and lithium ion batteries.
FAU - Bai, Linyi
AU  - Bai L
AD  - Division of Chemistry and Biological Chemistry, School of Physical and
      Mathematical Sciences, Nanyang Technological University, 21 Nanyang Link,
      Singapore 637371, Singapore. zhaoyanli@ntu.edu.sg.
FAU - Tu, Binbin
AU  - Tu B
AD  - Department of Chemistry and iChEM (Collaborative Innovation Center of Chemistry
      for Energy Materials), Fudan University, 220 Handan Road, Shanghai 200433, P. R. 
      China. qwli@fudan.edu.cn.
FAU - Qi, Yi
AU  - Qi Y
AD  - Department of Chemistry and iChEM (Collaborative Innovation Center of Chemistry
      for Energy Materials), Fudan University, 220 Handan Road, Shanghai 200433, P. R. 
      China. qwli@fudan.edu.cn.
FAU - Gao, Qiang
AU  - Gao Q
AD  - Division of Chemistry and Biological Chemistry, School of Physical and
      Mathematical Sciences, Nanyang Technological University, 21 Nanyang Link,
      Singapore 637371, Singapore. zhaoyanli@ntu.edu.sg.
FAU - Liu, Dong
AU  - Liu D
AD  - Division of Chemistry and Biological Chemistry, School of Physical and
      Mathematical Sciences, Nanyang Technological University, 21 Nanyang Link,
      Singapore 637371, Singapore. zhaoyanli@ntu.edu.sg.
FAU - Liu, Zhizhou
AU  - Liu Z
AD  - Division of Chemistry and Biological Chemistry, School of Physical and
      Mathematical Sciences, Nanyang Technological University, 21 Nanyang Link,
      Singapore 637371, Singapore. zhaoyanli@ntu.edu.sg.
FAU - Zhao, Lingzhi
AU  - Zhao L
AD  - Division of Chemistry and Biological Chemistry, School of Physical and
      Mathematical Sciences, Nanyang Technological University, 21 Nanyang Link,
      Singapore 637371, Singapore. zhaoyanli@ntu.edu.sg.
FAU - Li, Qiaowei
AU  - Li Q
AD  - Department of Chemistry and iChEM (Collaborative Innovation Center of Chemistry
      for Energy Materials), Fudan University, 220 Handan Road, Shanghai 200433, P. R. 
      China. qwli@fudan.edu.cn.
FAU - Zhao, Yanli
AU  - Zhao Y
AD  - Division of Chemistry and Biological Chemistry, School of Physical and
      Mathematical Sciences, Nanyang Technological University, 21 Nanyang Link,
      Singapore 637371, Singapore. zhaoyanli@ntu.edu.sg and School of Materials Science
      and Engineering, Nanyang Technological University, 50 Nanyang Avenue, Singapore
      639798, Singapore.
LA  - ENG
PT  - JOURNAL ARTICLE
DEP - 20160119
TA  - Chem Commun (Camb)
JT  - Chemical communications (Cambridge, England)
JID - 9610838
CRDT- 2016/01/20 06:00
AID - 10.1039/c5cc09935h [doi]
PST - aheadofprint
SO  - Chem Commun (Camb). 2016 Jan 19.
</pre>