<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<pre>
PMID- 26785447
OWN - NLM
STAT- Publisher
DA  - 20160119
LR  - 20160119
IS  - 1095-9203 (Electronic)
IS  - 0036-8075 (Linking)
VI  - 350
IP  - 6265
DP  - 2015 Dec 4
TI  - Japan's longevity challenge.
PG  - 1135
FAU - Akiyama, Hiroko
AU  - Akiyama H
AD  - Hiroko Akiyama is a professor at the Institute of Gerontology at the University
      of Tokyo, 7-3-1 Hongo, Bunkyoku, Tokyo, Japan. akiyama@iog.u-tokyo.ac.jp.
LA  - ENG
PT  - EDITORIAL
TA  - Science
JT  - Science (New York, N.Y.)
JID - 0404511
CRDT- 2016/01/20 06:00
AID - 350/6265/1135 [pii]
AID - 10.1126/science.aad9386 [doi]
PST - ppublish
SO  - Science. 2015 Dec 4;350(6265):1135.
</pre>