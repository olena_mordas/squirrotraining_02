<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<pre>
PMID- 26785494
OWN - NLM
STAT- Publisher
DA  - 20160119
LR  - 20160119
IS  - 1095-9203 (Electronic)
IS  - 0036-8075 (Linking)
VI  - 350
IP  - 6265
DP  - 2015 Dec 4
TI  - Imbalanced OPA1 processing and mitochondrial fragmentation cause heart failure in
      mice.
LID - aad0116 [pii]
AB  - Mitochondrial morphology is shaped by fusion and division of their membranes.
      Here, we found that adult myocardial function depends on balanced mitochondrial
      fusion and fission, maintained by processing of the dynamin-like guanosine
      triphosphatase OPA1 by the mitochondrial peptidases YME1L and OMA1.
      Cardiac-specific ablation of Yme1l in mice activated OMA1 and accelerated OPA1
      proteolysis, which triggered mitochondrial fragmentation and altered cardiac
      metabolism. This caused dilated cardiomyopathy and heart failure. Cardiac
      function and mitochondrial morphology were rescued by Oma1 deletion, which
      prevented OPA1 cleavage. Feeding mice a high-fat diet or ablating Yme1l in
      skeletal muscle restored cardiac metabolism and preserved heart function without 
      suppressing mitochondrial fragmentation. Thus, unprocessed OPA1 is sufficient to 
      maintain heart function, OMA1 is a critical regulator of cardiomyocyte survival, 
      and mitochondrial morphology and cardiac metabolism are intimately linked.
CI  - Copyright (c) 2015, American Association for the Advancement of Science.
FAU - Wai, Timothy
AU  - Wai T
AD  - Institute for Genetics, University of Cologne, 50674 Cologne, Germany.
      Max-Planck-Institute for Biology of Aging, Cologne, Germany.
FAU - Garcia-Prieto, Jaime
AU  - Garcia-Prieto J
AD  - Myocardial Pathophysiology Area, Centro Nacional de Investigaciones
      Cardiovasculares Carlos III (CNIC), Madrid, Spain.
FAU - Baker, Michael J
AU  - Baker MJ
AD  - Institute for Genetics, University of Cologne, 50674 Cologne, Germany.
FAU - Merkwirth, Carsten
AU  - Merkwirth C
AD  - Institute for Genetics, University of Cologne, 50674 Cologne, Germany.
FAU - Benit, Paule
AU  - Benit P
AD  - INSERM UMR 1141, Hopital Robert Debre, Paris, France. Universite Paris 7, Faculte
      de Medecine Denis Diderot, Paris, France.
FAU - Rustin, Pierre
AU  - Rustin P
AD  - INSERM UMR 1141, Hopital Robert Debre, Paris, France. Universite Paris 7, Faculte
      de Medecine Denis Diderot, Paris, France.
FAU - Ruperez, Francisco Javier
AU  - Ruperez FJ
AD  - Centre for Metabolomics and Bioanalysis (CEMBIO), Faculty of Pharmacy,
      Universidad San Pablo CEU, Campus Monteprincipe, Boadilla del Monte, 28668
      Madrid, Spain.
FAU - Barbas, Coral
AU  - Barbas C
AD  - Centre for Metabolomics and Bioanalysis (CEMBIO), Faculty of Pharmacy,
      Universidad San Pablo CEU, Campus Monteprincipe, Boadilla del Monte, 28668
      Madrid, Spain.
FAU - Ibanez, Borja
AU  - Ibanez B
AD  - Myocardial Pathophysiology Area, Centro Nacional de Investigaciones
      Cardiovasculares Carlos III (CNIC), Madrid, Spain. Department of Cardiology,
      Instituto de Investigacion Sanitaria (IIS), Fundacion Jimenez Diaz Hospital,
      Madrid, Spain. thomas.langer@uni-koeln.de bibanez@cnic.es.
FAU - Langer, Thomas
AU  - Langer T
AD  - Institute for Genetics, University of Cologne, 50674 Cologne, Germany.
      Max-Planck-Institute for Biology of Aging, Cologne, Germany. Cologne Excellence
      Cluster on Cellular Stress Responses in Aging-Associated Diseases (CECAD),
      University of Cologne, Cologne, Germany. Center for Molecular Medicine (CMMC),
      University of Cologne, Cologne, Germany. thomas.langer@uni-koeln.de
      bibanez@cnic.es.
LA  - ENG
PT  - JOURNAL ARTICLE
TA  - Science
JT  - Science (New York, N.Y.)
JID - 0404511
CRDT- 2016/01/20 06:00
AID - 350/6265/aad0116 [pii]
AID - 10.1126/science.aad0116 [doi]
PST - ppublish
SO  - Science. 2015 Dec 4;350(6265). pii: aad0116.
</pre>