<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<pre>
PMID- 26785445
OWN - NLM
STAT- Publisher
DA  - 20160119
LR  - 20160119
IS  - 1528-1175 (Electronic)
IS  - 0003-3022 (Linking)
VI  - 124
IP  - 2
DP  - 2016 Feb
TI  - Persistent Postoperative Cognitive Decline?: The Pyramid of Evidence.
PG  - A23
FAU - Avidan, Michael S
AU  - Avidan MS
AD  - Department of Anesthesiology, Washington University School of Medicine.
FAU - Evers, Alex S
AU  - Evers AS
LA  - ENG
PT  - JOURNAL ARTICLE
TA  - Anesthesiology
JT  - Anesthesiology
JID - 1300217
CRDT- 2016/01/20 06:00
AID - 10.1097/01.anes.0000476058.02255.81 [doi]
AID - 00000542-201602000-00005 [pii]
PST - ppublish
SO  - Anesthesiology. 2016 Feb;124(2):A23.
</pre>