<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<pre>
PMID- 26785436
OWN - NLM
STAT- Publisher
DA  - 20160119
LR  - 20160119
IS  - 1528-1175 (Electronic)
IS  - 0003-3022 (Linking)
VI  - 124
IP  - 2
DP  - 2016 Feb
TI  - In Reply.
PG  - 513-514
FAU - Lee, Susan M
AU  - Lee SM
AD  - University of California, San Francisco, San Francisco Veterans Affairs Medical
      Center, San Francisco, California (S.M.L.). susan.lee5@va.gov or
      suze.lee@utoronto.ca.
FAU - Takemoto, Steven
AU  - Takemoto S
FAU - Wallace, Arthur W
AU  - Wallace AW
LA  - ENG
PT  - JOURNAL ARTICLE
TA  - Anesthesiology
JT  - Anesthesiology
JID - 1300217
CRDT- 2016/01/20 06:00
AID - 10.1097/ALN.0000000000000965 [doi]
AID - 00000542-201602000-00043 [pii]
PST - ppublish
SO  - Anesthesiology. 2016 Feb;124(2):513-514.
</pre>