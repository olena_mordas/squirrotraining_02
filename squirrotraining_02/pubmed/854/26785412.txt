<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<pre>
PMID- 26785412
OWN - NLM
STAT- Publisher
DA  - 20160119
LR  - 20160119
IS  - 1468-5922 (Electronic)
IS  - 0021-8774 (Linking)
VI  - 61
IP  - 1
DP  - 2016 Feb
TI  - Working in the borderland: early relational trauma and Fordham's analysis of 'K'.
PG  - 44-62
LID - 10.1111/1468-5922.12191 [doi]
AB  - This paper outlines a view of early relational trauma as underlying borderline
      states of mind, and argues that Knox's paper on internal working models and the
      complex provides a basis for understanding such states of mind. The author argues
      that in addition to internal working models, the complex also embodies and
      contains primitive defences of the core self. He outlines how these apply on the 
      objective, subjective, transference and archetypal levels, and in direct and
      reversed forms and applies this to the account of Fordham's analysis of his
      patient 'K', which ended in impasse. The paper explores the dynamic that emerged 
      in that analysis and suggests that it could be helpfully accounted for in terms
      of the co-construction and re-construction of early relational trauma in the
      analytic relationship.
CI  - (c) 2016, The Society of Analytical Psychology.
FAU - West, Marcus
AU  - West M
AD  - Findon, West Sussex.
LA  - ENG
PT  - JOURNAL ARTICLE
TA  - J Anal Psychol
JT  - The Journal of analytical psychology
JID - 0376573
OTO - NOTNLM
OT  - : , , , , , ,
OT  - Idealisierung
OT  - Kernselbst
OT  - Ko-Konstruktion
OT  - Komplex
OT  - Re-Konstruktion
OT  - Relationaler Ansatz
OT  - abordaje relacional
OT  - approccio relazionale
OT  - approche relationnelle
OT  - centro del se
OT  - co-construccion
OT  - co-construction
OT  - cocostruzione
OT  - complejo
OT  - complex
OT  - complexe
OT  - core self
OT  - early relational trauma
OT  - fruhes Beziehungstrauma
OT  - idealizacion
OT  - idealization
OT  - idealizzazione
OT  - idealisation
OT  - noyau du soi
OT  - re-construccion
OT  - re-construction
OT  - relational approach
OT  - ricostruzione
OT  - self nuclear
OT  - trauma relacional temprano
OT  - trauma relazionale precoce complesso
OT  - traumatisme relationnel precoce
OT  - small ve, Cyrillicsmall o, Cyrillicsmall es, Cyrillic-small es, Cyrillicsmall o, 
      Cyrillicsmall ze, Cyrillicsmall de, Cyrillicsmall a, Cyrillicsmall en,
      Cyrillicsmall i, Cyrillicsmall ie, Cyrillic
OT  - small i, Cyrillicsmall de, Cyrillicsmall ie, Cyrillicsmall a, Cyrillicsmall el,
      Cyrillicsmall i, Cyrillicsmall ze, Cyrillicsmall a, Cyrillicsmall tse,
      Cyrillicsmall i, Cyrillicsmall ya, Cyrillic
OT  - small ka, Cyrillicsmall o, Cyrillicsmall em, Cyrillicsmall pe, Cyrillicsmall el, 
      Cyrillicsmall ie, Cyrillicsmall ka, Cyrillicsmall es, Cyrillic
OT  - small o, Cyrillicsmall te, Cyrillicsmall en, Cyrillicsmall o, Cyrillicsmall sha, 
      Cyrillicsmall ie, Cyrillicsmall en, Cyrillicsmall che, Cyrillicsmall ie,
      Cyrillicsmall es, Cyrillicsmall ka, Cyrillicsmall i, Cyrillicsmall short i,
      Cyrillic small pe, Cyrillicsmall o, Cyrillicsmall de, Cyrillicsmall ha,
      Cyrillicsmall o, Cyrillicsmall de, Cyrillic
OT  - small er, Cyrillicsmall a, Cyrillicsmall en, Cyrillicsmall en, Cyrillicsmall ya, 
      Cyrillicsmall ya, Cyrillic small te, Cyrillicsmall er, Cyrillicsmall a,
      Cyrillicsmall ve, Cyrillicsmall em, Cyrillicsmall a, Cyrillic small o,
      Cyrillicsmall te, Cyrillicsmall en, Cyrillicsmall o, Cyrillicsmall sha,
      Cyrillicsmall ie, Cyrillicsmall en, Cyrillicsmall i, Cyrillicsmall short i,
      Cyrillic
OT  - small es, Cyrillicsmall o, Cyrillic-small es, Cyrillicsmall o, Cyrillicsmall ze, 
      Cyrillicsmall i, Cyrillicsmall de, Cyrillicsmall a, Cyrillicsmall en,
      Cyrillicsmall i, Cyrillicsmall ie, Cyrillic
OT  - small ya, Cyrillicsmall de, Cyrillicsmall ie, Cyrillicsmall er, Cyrillicsmall en,
      Cyrillicsmall a, Cyrillicsmall ya, Cyrillic small es, Cyrillicsmall a,
      Cyrillicsmall em, Cyrillicsmall o, Cyrillicsmall es, Cyrillicsmall te,
      Cyrillicsmall soft sign, Cyrillic
CRDT- 2016/01/20 06:00
PHST- 2015/08/06 [received]
PHST- 2015/11/02 [accepted]
AID - 10.1111/1468-5922.12191 [doi]
PST - ppublish
SO  - J Anal Psychol. 2016 Feb;61(1):44-62. doi: 10.1111/1468-5922.12191.
</pre>