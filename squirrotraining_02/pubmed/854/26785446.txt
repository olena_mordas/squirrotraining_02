<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<pre>
PMID- 26785446
OWN - NLM
STAT- Publisher
DA  - 20160119
LR  - 20160119
IS  - 1944-8252 (Electronic)
IS  - 1944-8244 (Linking)
DP  - 2016 Jan 19
TI  - Highly Luminescent Solution-Grown Thiophene-Phenylene Co-Oligomer Single
      Crystals.
AB  - Thiophene-phenylene co-oligomers (TPCOs) are among the most promising materials
      for organic light emitting devices. Here we report on record high among TPCO
      single crystals photoluminescence quantum yield reaching 60%. The solution-grown 
      crystals are stronger luminescent than the vapor-grown ones, in contrast to a
      common believe that the vapor-processed organic electronic materials show the
      highest performance. We also demonstrate that the solution-grown TPCO single
      crystals perform in organic field effect transistors as good as the vapor-grown
      ones. Altogether, the solution-grown TPCO crystals are demonstrated to hold great
      potential for organic electronics.
FAU - Kudryashova, Lyudmila Gennadyevna
AU  - Kudryashova LG
FAU - Kazantsev, Maxim Sergeevich
AU  - Kazantsev MS
FAU - Postnikov, Valery Anatolievich
AU  - Postnikov VA
FAU - Bruevich, Vladimir Vasilievich
AU  - Bruevich VV
FAU - Luponosov, Yuriy N
AU  - Luponosov YN
FAU - Surin, Nikolay M
AU  - Surin NM
FAU - Borshchev, Oleg V
AU  - Borshchev OV
FAU - Ponomarenko, Sergei A
AU  - Ponomarenko SA
FAU - Pshenichnikov, Maxim S
AU  - Pshenichnikov MS
FAU - Paraschuk, Dmitry Yurievich
AU  - Paraschuk DY
LA  - ENG
PT  - JOURNAL ARTICLE
DEP - 20160119
TA  - ACS Appl Mater Interfaces
JT  - ACS applied materials &amp; interfaces
JID - 101504991
CRDT- 2016/01/20 06:00
AID - 10.1021/acsami.5b11967 [doi]
PST - aheadofprint
SO  - ACS Appl Mater Interfaces. 2016 Jan 19.
</pre>