<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<pre>
PMID- 26785462
OWN - NLM
STAT- Publisher
DA  - 20160119
LR  - 20160119
IS  - 1095-9203 (Electronic)
IS  - 0036-8075 (Linking)
VI  - 350
IP  - 6265
DP  - 2015 Dec 4
TI  - Classical entanglement?
PG  - 1172-1173
FAU - Karimi, Ebrahim
AU  - Karimi E
AD  - Department of Physics and Max Planck Centre for Extreme and Quantum Photonics,
      University of Ottawa, Ottawa, Ontario K1N 6N5, Canada. ekarimi@uottawa.ca
      boydrw@mac.com.
FAU - Boyd, Robert W
AU  - Boyd RW
AD  - Department of Physics and Max Planck Centre for Extreme and Quantum Photonics,
      University of Ottawa, Ottawa, Ontario K1N 6N5, Canada. Institute of Optics,
      University of Rochester, Rochester, NY 14627, USA. ekarimi@uottawa.ca
      boydrw@mac.com.
LA  - ENG
PT  - JOURNAL ARTICLE
TA  - Science
JT  - Science (New York, N.Y.)
JID - 0404511
CRDT- 2016/01/20 06:00
AID - 350/6265/1172 [pii]
AID - 10.1126/science.aad7174 [doi]
PST - ppublish
SO  - Science. 2015 Dec 4;350(6265):1172-1173.
</pre>