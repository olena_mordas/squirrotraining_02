<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<pre>
PMID- 26785401
OWN - NLM
STAT- Publisher
DA  - 20160119
LR  - 20160119
IS  - 1552-4930 (Electronic)
IS  - 1552-4922 (Linking)
DP  - 2016 Jan 19
TI  - Glycogen content in hepatocytes is related with their size in normal rat liver
      but not in cirrhotic one.
LID - 10.1002/cyto.a.22811 [doi]
AB  - BACKGROUND &amp; AIMS: Hepatocytes differ from one another by the degree of the
      ploidy, size, position in the liver lobule, and level of the DNA-synthetic
      processes. It is believed, that the cell size exerts substantial influence on the
      metabolism of the hepatocytes and the glycogen content in them. The aim of the
      present study was to test this hypothesis. METHODS: Dry weight of hepatocytes,
      their ploidy and glycogen content were determined in the normal and the cirrhotic
      rat liver. Liver cirrhosis in rats was produced by chronic inhalation of CCl4
      vapours in the course of 6 months. A combined cytophotometric method was used.
      Dry weight of the cell, its glycogen and DNA content were successively measured
      on a mapped preparation. RESULT: Hepatocytes of each ploidy class in the normal
      and the cirrhotic rat liver accumulated glycogen at the same rate. In the normal 
      liver, there was a distinct correlation between the size of hepatocytes and
      glycogen content in them. This correlation was observed in each ploidy class, and
      was especially pronounced in the class of mononucleate tetraploid hepatocytes. In
      the cirrhotic liver, there was no correlation between the size of the cells and
      their glycogen content. CONCLUSIONS: The impairment of liver lobular structure
      probably explains the observed lack of correlation between hepatocyte size and
      their glycogen content in the cirrhotic liver. (c) 2016 International Society for
      Advancement of Cytometry.
CI  - (c) 2016 International Society for Advancement of Cytometry.
FAU - Bezborodkina, Natalia N
AU  - Bezborodkina NN
AD  - Laboratory of cellular pathology, Institute of Cytology of the Russian Academy of
      Sciences, Saint Petersburg, Russia.
FAU - Chestnova, Anna Yu
AU  - Chestnova AY
AD  - Laboratory of cellular pathology, Institute of Cytology of the Russian Academy of
      Sciences, Saint Petersburg, Russia.
FAU - Vorobev, Mikhail L
AU  - Vorobev ML
AD  - Laboratory of cellular pathology, Institute of Cytology of the Russian Academy of
      Sciences, Saint Petersburg, Russia.
FAU - Kudryavtsev, Boris N
AU  - Kudryavtsev BN
AD  - Laboratory of cellular pathology, Institute of Cytology of the Russian Academy of
      Sciences, Saint Petersburg, Russia.
LA  - ENG
PT  - JOURNAL ARTICLE
DEP - 20160119
TA  - Cytometry A
JT  - Cytometry. Part A : the journal of the International Society for Analytical
      Cytology
JID - 101235694
OTO - NOTNLM
OT  - dry weight of hepatocytes
OT  - glycogen
OT  - hepatocytes
OT  - liver cirrhosis
OT  - ploidy
CRDT- 2016/01/20 06:00
PHST- 2015/04/03 [received]
PHST- 2015/11/27 [revised]
PHST- 2015/12/10 [accepted]
AID - 10.1002/cyto.a.22811 [doi]
PST - aheadofprint
SO  - Cytometry A. 2016 Jan 19. doi: 10.1002/cyto.a.22811.
</pre>