<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<pre>
PMID- 26785403
OWN - NLM
STAT- Publisher
DA  - 20160119
LR  - 20160119
IS  - 1097-0223 (Electronic)
IS  - 0197-3851 (Linking)
DP  - 2016 Jan 19
TI  - Follow-up of Multiple Aneuploidies and Single Monosomies Detected by Noninvasive 
      Prenatal Testing: Implications for Management and Counseling.
LID - 10.1002/pd.4778 [doi]
AB  - OBJECTIVE: To determine the underlying biological basis for NIPT results of
      multiple aneuploidies or autosomal monosomies. METHODS: Retrospective analysis of
      113,415 tests to determine the study cohort, consisting of 138 (0.12%) cases
      reported as a single autosomal monosomy (n=65), single trisomy with a sex
      chromosome aneuploidy (n=36), or with multiple aneuploidies (n=37). Clinical
      outcome information was reviewed and stratified into eight categories according
      to whether the karyotype or sonographic information agreed or disagreed with
      sequencing results. RESULTS: Of 67 cases with fetal or neonatal karyotypes
      available, 16 (24%) were partially or fully concordant with the NIPT result, 4
      (6%) had aneuploidy on a reference chromosome, and 47 (70%) had normal fetal
      chromosomes, in which 5/47 had maternal malignancies reported. One case of
      maternal mosaic trisomy 8 was also detected. Of cases with no fetal karyotype
      information, ten had an abnormal clinical outcome, one was a normal live birth,
      and one reported maternal malignancy. CONCLUSIONS: Noninvasive prenatal test
      results of autosomal monosomy or multiple aneuploidies are rare, but have a
      diversity of underlying biologic causes. Some reflect the fetal karyotype, some
      reflect the presence of other maternal or fetal chromosome abnormalities, and a
      small number are linked to maternal disease.
CI  - This article is protected by copyright. All rights reserved.
FAU - Snyder, Holly L
AU  - Snyder HL
AD  - Illumina, Inc., Redwood City, CA, USA.
FAU - Curnow, Kirsten J
AU  - Curnow KJ
AD  - Illumina, Inc., Redwood City, CA, USA.
FAU - Bhatt, Sucheta
AU  - Bhatt S
AD  - Illumina, Inc., Redwood City, CA, USA.
FAU - Bianchi, Diana W
AU  - Bianchi DW
AUID- ORCID: http://orcid.org/0000-0001-9270-5267
AD  - Mother Infant Research Institute, Tufts Medical Center, Boston, MA, USA.
LA  - ENG
PT  - JOURNAL ARTICLE
DEP - 20160119
TA  - Prenat Diagn
JT  - Prenatal diagnosis
JID - 8106540
CRDT- 2016/01/20 06:00
PHST- 2015/11/24 [received]
PHST- 2016/01/12 [revised]
PHST- 2016/01/15 [accepted]
AID - 10.1002/pd.4778 [doi]
PST - aheadofprint
SO  - Prenat Diagn. 2016 Jan 19. doi: 10.1002/pd.4778.
</pre>