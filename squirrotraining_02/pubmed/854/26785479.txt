<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<pre>
PMID- 26785479
OWN - NLM
STAT- Publisher
DA  - 20160119
LR  - 20160119
IS  - 1095-9203 (Electronic)
IS  - 0036-8075 (Linking)
VI  - 350
IP  - 6265
DP  - 2015 Dec 4
TI  - Mitochondrial dysfunction and longevity in animals: Untangling the knot.
PG  - 1204-1207
AB  - Mitochondria generate adenosine 5'-triphosphate (ATP) and are a source of
      potentially toxic reactive oxygen species (ROS). It has been suggested that the
      gradual mitochondrial dysfunction that is observed to accompany aging could in
      fact be causal to the aging process. Here we review findings that suggest that
      age-dependent mitochondrial dysfunction is not sufficient to limit life span.
      Furthermore, mitochondrial ROS are not always deleterious and can even stimulate 
      pro-longevity pathways. Thus, mitochondrial dysfunction plays a complex role in
      regulating longevity.
CI  - Copyright (c) 2015, American Association for the Advancement of Science.
FAU - Wang, Ying
AU  - Wang Y
AD  - Department of Biology, McGill University, Montreal, Quebec H3A 1B1, Canada.
FAU - Hekimi, Siegfried
AU  - Hekimi S
AD  - Department of Biology, McGill University, Montreal, Quebec H3A 1B1, Canada.
      siegfried.hekimi@mcgill.ca.
LA  - ENG
PT  - REVIEW
PT  - JOURNAL ARTICLE
TA  - Science
JT  - Science (New York, N.Y.)
JID - 0404511
CRDT- 2016/01/20 06:00
AID - 350/6265/1204 [pii]
AID - 10.1126/science.aac4357 [doi]
PST - ppublish
SO  - Science. 2015 Dec 4;350(6265):1204-1207.
</pre>