<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<pre>
PMID- 26785430
OWN - NLM
STAT- Publisher
DA  - 20160119
LR  - 20160119
IS  - 1528-1175 (Electronic)
IS  - 0003-3022 (Linking)
VI  - 124
IP  - 2
DP  - 2016 Feb
TI  - Cognitive Functioning after Surgery in Middle-aged and Elderly Danish Twins.
PG  - 312-321
AB  - BACKGROUND: Postoperative cognitive dysfunction is common, but it remains unclear
      whether there are long-term adverse cognitive effects of surgery combined with
      anesthesia. The authors examined the association between exposure to surgery and 
      level of cognitive functioning in a sample of 8,503 middle-aged and elderly
      twins. METHODS: Results from five cognitive tests were compared in twins exposed 
      to surgery, classified as major, minor, hip and knee replacement, or other, with 
      those of a reference group without surgery using linear regression adjusted for
      sex and age. Genetic and shared environmental confounding was addressed in
      intrapair analyses of 87 monozygotic and 124 dizygotic same-sexed twin pairs in
      whom one had a history of major surgery and the other did not. RESULTS:
      Statistically significantly lower composite cognitive score was found in twins
      with at least one major surgery compared with the reference group (mean
      difference, -0.27; 95% CI, -0.48 to -0.06), corresponding to one tenth of an SD, 
      that is, a negligible effect size. In the intrapair analysis, the surgery-exposed
      co-twin had the lower cognitive score in 49% (95% CI, 42 to 56%) of the pairs.
      None of the other groups differed from the reference group except the knee and
      hip replacement group that tended to have higher cognitive scores (mean
      difference, 0.35; 95% CI, -0.18 to 0.87). CONCLUSIONS: A history of major surgery
      was associated with a negligibly lower level of cognitive functioning. The
      supplementary analyses suggest that preoperative cognitive functioning and
      underlying diseases were more important for cognitive functioning in mid- and
      late life than surgery and anesthesia.
FAU - Dokkedal, Unni
AU  - Dokkedal U
AD  - From the Unit of Epidemiology, Biostatistics, and Biodemography, and the Danish
      Twin Registry, and the Danish Aging Research Center, Department of Public Health,
      University of Southern Denmark, Odense, Denmark (U.D., J.M.-F., K.C.);
      Departments of Anaesthesiology and Intensive Care Medicine (T.G.H.), Clinical
      Biochemistry and Pharmacology (K.C.), and Clinical Genetics (J.M.-F., K.C.),
      Odense University Hospital, Odense, Denmark; and Department of Anaesthesia,
      Centre of Head and Orthopaedics, Rigshospitalet, University of Copenhagen,
      Copenhagen, Denmark (L.S.R.).
FAU - Hansen, Tom G
AU  - Hansen TG
FAU - Rasmussen, Lars S
AU  - Rasmussen LS
FAU - Mengel-From, Jonas
AU  - Mengel-From J
FAU - Christensen, Kaare
AU  - Christensen K
LA  - ENG
PT  - JOURNAL ARTICLE
TA  - Anesthesiology
JT  - Anesthesiology
JID - 1300217
CRDT- 2016/01/20 06:00
AID - 10.1097/ALN.0000000000000957 [doi]
AID - 00000542-201602000-00016 [pii]
PST - ppublish
SO  - Anesthesiology. 2016 Feb;124(2):312-321.
</pre>