<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<pre>
PMID- 26785487
OWN - NLM
STAT- Publisher
DA  - 20160119
LR  - 20160119
IS  - 1095-9203 (Electronic)
IS  - 0036-8075 (Linking)
VI  - 350
IP  - 6265
DP  - 2015 Dec 4
TI  - Resolved magnetic-field structure and variability near the event horizon of
      Sagittarius A.
PG  - 1242-1245
AB  - Near a black hole, differential rotation of a magnetized accretion disk is
      thought to produce an instability that amplifies weak magnetic fields, driving
      accretion and outflow. These magnetic fields would naturally give rise to the
      observed synchrotron emission in galaxy cores and to the formation of
      relativistic jets, but no observations to date have been able to resolve the
      expected horizon-scale magnetic-field structure. We report interferometric
      observations at 1.3-millimeter wavelength that spatially resolve the linearly
      polarized emission from the Galactic Center supermassive black hole, Sagittarius 
      A*. We have found evidence for partially ordered magnetic fields near the event
      horizon, on scales of ~6 Schwarzschild radii, and we have detected and localized 
      the intrahour variability associated with these fields.
CI  - Copyright (c) 2015, American Association for the Advancement of Science.
FAU - Johnson, Michael D
AU  - Johnson MD
AD  - Harvard-Smithsonian Center for Astrophysics, 60 Garden Street, Cambridge, MA
      02138, USA. mjohnson@cfa.harvard.edu.
FAU - Fish, Vincent L
AU  - Fish VL
AD  - Haystack Observatory, Route 40, Massachusetts Institute of Technology, Westford, 
      MA 01886, USA.
FAU - Doeleman, Sheperd S
AU  - Doeleman SS
AD  - Harvard-Smithsonian Center for Astrophysics, 60 Garden Street, Cambridge, MA
      02138, USA. Haystack Observatory, Route 40, Massachusetts Institute of
      Technology, Westford, MA 01886, USA.
FAU - Marrone, Daniel P
AU  - Marrone DP
AD  - Steward Observatory, University of Arizona, 933 North Cherry Avenue, Tucson, AZ
      85721-0065, USA.
FAU - Plambeck, Richard L
AU  - Plambeck RL
AD  - Department of Astronomy, Radio Astronomy Laboratory, 501 Campbell, University of 
      California Berkeley, Berkeley, CA 94720-3411, USA.
FAU - Wardle, John F C
AU  - Wardle JF
AD  - Department of Physics MS-057, Brandeis University, Waltham, MA 02454-0911.
FAU - Akiyama, Kazunori
AU  - Akiyama K
AD  - Haystack Observatory, Route 40, Massachusetts Institute of Technology, Westford, 
      MA 01886, USA. National Astronomical Observatory of Japan, Osawa 2-21-1, Mitaka, 
      Tokyo 181-8588, Japan. Department of Astronomy, Graduate School of Science, The
      University of Tokyo, 7-3-1 Hongo, Bunkyo-ku, Tokyo 113-0033, Japan.
FAU - Asada, Keiichi
AU  - Asada K
AD  - Institute of Astronomy and Astrophysics, Academia Sinica, Post Office Box 23-141,
      Taipei 10617, Taiwan.
FAU - Beaudoin, Christopher
AU  - Beaudoin C
AD  - Haystack Observatory, Route 40, Massachusetts Institute of Technology, Westford, 
      MA 01886, USA.
FAU - Blackburn, Lindy
AU  - Blackburn L
AD  - Harvard-Smithsonian Center for Astrophysics, 60 Garden Street, Cambridge, MA
      02138, USA.
FAU - Blundell, Ray
AU  - Blundell R
AD  - Harvard-Smithsonian Center for Astrophysics, 60 Garden Street, Cambridge, MA
      02138, USA.
FAU - Bower, Geoffrey C
AU  - Bower GC
AD  - Academia Sinica Institute for Astronomy and Astrophysics (ASIAA), 645 N. A'ohoku 
      Pl. Hilo, HI 96720, USA.
FAU - Brinkerink, Christiaan
AU  - Brinkerink C
AD  - Department of Astrophysics/Institute for Mathematics, Astrophysics and Particle
      Physics, Radboud University Nijmegen, Post Office Box 9010, 6500 GL Nijmegen,
      Netherlands.
FAU - Broderick, Avery E
AU  - Broderick AE
AD  - Perimeter Institute for Theoretical Physics, 31 Caroline Street North, Waterloo, 
      ON N2L 2Y5, Canada. Department of Physics and Astronomy, University of Waterloo, 
      200 University Avenue West, Waterloo, ON N2L 3G1, Canada.
FAU - Cappallo, Roger
AU  - Cappallo R
AD  - Haystack Observatory, Route 40, Massachusetts Institute of Technology, Westford, 
      MA 01886, USA.
FAU - Chael, Andrew A
AU  - Chael AA
AD  - Harvard-Smithsonian Center for Astrophysics, 60 Garden Street, Cambridge, MA
      02138, USA.
FAU - Crew, Geoffrey B
AU  - Crew GB
AD  - Haystack Observatory, Route 40, Massachusetts Institute of Technology, Westford, 
      MA 01886, USA.
FAU - Dexter, Jason
AU  - Dexter J
AD  - Max Planck Institute for Extraterrestrial Physics, Giessenbachstrasse 1, 85748
      Garching, Germany.
FAU - Dexter, Matt
AU  - Dexter M
AD  - Department of Astronomy, Radio Astronomy Laboratory, 501 Campbell, University of 
      California Berkeley, Berkeley, CA 94720-3411, USA.
FAU - Freund, Robert
AU  - Freund R
AD  - Steward Observatory, University of Arizona, 933 North Cherry Avenue, Tucson, AZ
      85721-0065, USA.
FAU - Friberg, Per
AU  - Friberg P
AD  - James Clerk Maxwell Telescope, East Asia Observatory, 660 N. A'ohoku Place,
      University Park, Hilo, HI 96720, USA.
FAU - Gold, Roman
AU  - Gold R
AD  - Department of Physics, Joint Space-Science Institute, University of Maryland at
      College Park, Physical Sciences Complex, College Park, MD 20742, USA.
FAU - Gurwell, Mark A
AU  - Gurwell MA
AD  - Harvard-Smithsonian Center for Astrophysics, 60 Garden Street, Cambridge, MA
      02138, USA.
FAU - Ho, Paul T P
AU  - Ho PT
AD  - Institute of Astronomy and Astrophysics, Academia Sinica, Post Office Box 23-141,
      Taipei 10617, Taiwan.
FAU - Honma, Mareki
AU  - Honma M
AD  - National Astronomical Observatory of Japan, Osawa 2-21-1, Mitaka, Tokyo 181-8588,
      Japan. Graduate University for Advanced Studies, Mitaka, 2-21-1 Osawa, Mitaka,
      Tokyo 181-8588.
FAU - Inoue, Makoto
AU  - Inoue M
AD  - Institute of Astronomy and Astrophysics, Academia Sinica, Post Office Box 23-141,
      Taipei 10617, Taiwan.
FAU - Kosowsky, Michael
AU  - Kosowsky M
AD  - Harvard-Smithsonian Center for Astrophysics, 60 Garden Street, Cambridge, MA
      02138, USA. Haystack Observatory, Route 40, Massachusetts Institute of
      Technology, Westford, MA 01886, USA. Department of Physics MS-057, Brandeis
      University, Waltham, MA 02454-0911.
FAU - Krichbaum, Thomas P
AU  - Krichbaum TP
AD  - Max-Planck-Institut fur Radioastronomie, Auf dem Hugel 69, D-53121 Bonn, Germany.
FAU - Lamb, James
AU  - Lamb J
AD  - Owens Valley Radio Observatory, California Institute of Technology, 100 Leighton 
      Lane, Big Pine, CA 93513-0968, USA.
FAU - Loeb, Abraham
AU  - Loeb A
AD  - Harvard-Smithsonian Center for Astrophysics, 60 Garden Street, Cambridge, MA
      02138, USA.
FAU - Lu, Ru-Sen
AU  - Lu RS
AD  - Haystack Observatory, Route 40, Massachusetts Institute of Technology, Westford, 
      MA 01886, USA. Max-Planck-Institut fur Radioastronomie, Auf dem Hugel 69, D-53121
      Bonn, Germany.
FAU - MacMahon, David
AU  - MacMahon D
AD  - Department of Astronomy, Radio Astronomy Laboratory, 501 Campbell, University of 
      California Berkeley, Berkeley, CA 94720-3411, USA.
FAU - McKinney, Jonathan C
AU  - McKinney JC
AD  - Department of Physics, Joint Space-Science Institute, University of Maryland at
      College Park, Physical Sciences Complex, College Park, MD 20742, USA.
FAU - Moran, James M
AU  - Moran JM
AD  - Harvard-Smithsonian Center for Astrophysics, 60 Garden Street, Cambridge, MA
      02138, USA.
FAU - Narayan, Ramesh
AU  - Narayan R
AD  - Harvard-Smithsonian Center for Astrophysics, 60 Garden Street, Cambridge, MA
      02138, USA.
FAU - Primiani, Rurik A
AU  - Primiani RA
AD  - Harvard-Smithsonian Center for Astrophysics, 60 Garden Street, Cambridge, MA
      02138, USA.
FAU - Psaltis, Dimitrios
AU  - Psaltis D
AD  - Steward Observatory, University of Arizona, 933 North Cherry Avenue, Tucson, AZ
      85721-0065, USA.
FAU - Rogers, Alan E E
AU  - Rogers AE
AD  - Haystack Observatory, Route 40, Massachusetts Institute of Technology, Westford, 
      MA 01886, USA.
FAU - Rosenfeld, Katherine
AU  - Rosenfeld K
AD  - Harvard-Smithsonian Center for Astrophysics, 60 Garden Street, Cambridge, MA
      02138, USA.
FAU - SooHoo, Jason
AU  - SooHoo J
AD  - Haystack Observatory, Route 40, Massachusetts Institute of Technology, Westford, 
      MA 01886, USA.
FAU - Tilanus, Remo P J
AU  - Tilanus RP
AD  - Department of Astrophysics/Institute for Mathematics, Astrophysics and Particle
      Physics, Radboud University Nijmegen, Post Office Box 9010, 6500 GL Nijmegen,
      Netherlands. Leiden Observatory, Leiden University, Post Office Box 9513, 2300 RA
      Leiden, Netherlands.
FAU - Titus, Michael
AU  - Titus M
AD  - Haystack Observatory, Route 40, Massachusetts Institute of Technology, Westford, 
      MA 01886, USA.
FAU - Vertatschitsch, Laura
AU  - Vertatschitsch L
AD  - Harvard-Smithsonian Center for Astrophysics, 60 Garden Street, Cambridge, MA
      02138, USA.
FAU - Weintroub, Jonathan
AU  - Weintroub J
AD  - Harvard-Smithsonian Center for Astrophysics, 60 Garden Street, Cambridge, MA
      02138, USA.
FAU - Wright, Melvyn
AU  - Wright M
AD  - Department of Astronomy, Radio Astronomy Laboratory, 501 Campbell, University of 
      California Berkeley, Berkeley, CA 94720-3411, USA.
FAU - Young, Ken H
AU  - Young KH
AD  - Harvard-Smithsonian Center for Astrophysics, 60 Garden Street, Cambridge, MA
      02138, USA.
FAU - Zensus, J Anton
AU  - Zensus JA
AD  - Max-Planck-Institut fur Radioastronomie, Auf dem Hugel 69, D-53121 Bonn, Germany.
FAU - Ziurys, Lucy M
AU  - Ziurys LM
AD  - Steward Observatory, University of Arizona, 933 North Cherry Avenue, Tucson, AZ
      85721-0065, USA.
LA  - ENG
PT  - JOURNAL ARTICLE
DEP - 20151203
TA  - Science
JT  - Science (New York, N.Y.)
JID - 0404511
CRDT- 2016/01/20 06:00
PHST- 2015/06/11 [received]
PHST- 2015/10/13 [accepted]
PHST- 2015/12/03 [aheadofprint]
AID - 350/6265/1242 [pii]
AID - 10.1126/science.aac7087 [doi]
PST - ppublish
SO  - Science. 2015 Dec 4;350(6265):1242-1245. Epub 2015 Dec 3.
</pre>