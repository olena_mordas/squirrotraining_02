<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<pre>
PMID- 26785414
OWN - NLM
STAT- Publisher
DA  - 20160119
LR  - 20160119
IS  - 1468-5922 (Electronic)
IS  - 0021-8774 (Linking)
VI  - 61
IP  - 1
DP  - 2016 Feb
TI  - Synchronistic mind-matter correlations in therapeutic practice: a commentary on
      Connolly (2015).
PG  - 79-85
LID - 10.1111/1468-5922.12196 [doi]
AB  - This commentary adds some ideas and refinements to the inspiring discussion in a 
      recent paper by Connolly () that makes use of a dual-aspect framework developed
      by us earlier. One key point is that exceptional experiences (of which
      synchronicities are a special case) cannot in general be identified with
      experiences of non-categorial or acategorial mental states. In fact, most
      exceptional experiences reported in the literature are experiences of categorial 
      states. Conversely, there are non-categorial and acategorial states whose
      experience is not exceptional. Moreover, the psychodynamics of a synchronistic
      experience contain a subtle mesh of interacting processes pertaining to
      categorial, non-categorial and acategorial domains. We outline how this mesh may 
      be addressed in particular cases of synchronicity described by Connolly.
CI  - (c) 2016, The Society of Analytical Psychology.
FAU - Atmanspacher, Harald
AU  - Atmanspacher H
AD  - Zurich and Freiburg.
FAU - Fach, Wolfgang
AU  - Fach W
AD  - Zurich and Freiburg.
LA  - ENG
PT  - JOURNAL ARTICLE
TA  - J Anal Psychol
JT  - The Journal of analytical psychology
JID - 0376573
OTO - NOTNLM
OT  - Geist-Materiebeziehungen
OT  - Synchronizitat
OT  - acategorial states
OT  - akategoriale Zustande
OT  - aspecto-dual del monismo
OT  - aussergewohnliche Erfahrungen
OT  - correlaciones mente-materia
OT  - correlazioni mente-materia
OT  - correlations entre l'esprit et la matiere
OT  - dual-aspect monism
OT  - duale-Aspekte Monismus
OT  - esperienze eccezionali
OT  - estados acategoriales
OT  - estados no-categoriales
OT  - exceptional experiences
OT  - experiencias excepcionales
OT  - experiences exceptionnelles
OT  - mind-matter correlations
OT  - monisme a double aspect
OT  - monismo dal duplice aspetto
OT  - nicht kategoriale Zustande
OT  - non-categorial states
OT  - sincronicidad
OT  - sincronicita
OT  - stati acategoriali
OT  - stati non categoriali
OT  - synchronicity
OT  - synchronicite
OT  - etats acategoriels
OT  - etats non-categoriels
OT  - small a, Cyrillicsmall ka, Cyrillicsmall a, Cyrillicsmall te, Cyrillicsmall ie,
      Cyrillicsmall ghe, Cyrillicsmall o, Cyrillicsmall er, Cyrillicsmall i,
      Cyrillicsmall a, Cyrillicsmall el, Cyrillicsmall soft sign, Cyrillicsmall en,
      Cyrillicsmall yeru, Cyrillicsmall ie, Cyrillic small es, Cyrillicsmall o,
      Cyrillicsmall es, Cyrillicsmall te, Cyrillicsmall o, Cyrillicsmall ya,
      Cyrillicsmall en, Cyrillicsmall i, Cyrillicsmall ya, Cyrillic
OT  - small de, Cyrillicsmall ve, Cyrillicsmall u, Cyrillicsmall ha, Cyrillic-small a, 
      Cyrillicsmall es, Cyrillicsmall pe, Cyrillicsmall ie, Cyrillicsmall ka,
      Cyrillicsmall te, Cyrillicsmall en, Cyrillicsmall yeru, Cyrillicsmall short i,
      Cyrillic small em, Cyrillicsmall o, Cyrillicsmall en, Cyrillicsmall i,
      Cyrillicsmall ze, Cyrillicsmall em, Cyrillic
OT  - small i, Cyrillicsmall es, Cyrillicsmall ka, Cyrillicsmall el, Cyrillicsmall yu, 
      Cyrillicsmall che, Cyrillicsmall i, Cyrillicsmall te, Cyrillicsmall ie,
      Cyrillicsmall el, Cyrillicsmall soft sign, Cyrillicsmall en, Cyrillicsmall yeru, 
      Cyrillicsmall ie, Cyrillic small pe, Cyrillicsmall ie, Cyrillicsmall er,
      Cyrillicsmall ie, Cyrillicsmall zhe, Cyrillicsmall i, Cyrillicsmall ve,
      Cyrillicsmall a, Cyrillicsmall en, Cyrillicsmall i, Cyrillicsmall ya, Cyrillic
OT  - small ka, Cyrillicsmall o, Cyrillicsmall er, Cyrillicsmall er, Cyrillicsmall ie, 
      Cyrillicsmall el, Cyrillicsmall ya, Cyrillicsmall tse, Cyrillicsmall i,
      Cyrillicsmall i, Cyrillic &lt;&lt;small u, Cyrillicsmall em, Cyrillic-small em,
      Cyrillicsmall a, Cyrillicsmall te, Cyrillicsmall ie, Cyrillicsmall er,
      Cyrillicsmall i, Cyrillicsmall ya, Cyrillic&gt;&gt;
OT  - small en, Cyrillicsmall ie, Cyrillic-small ka, Cyrillicsmall a, Cyrillicsmall te,
      Cyrillicsmall ie, Cyrillicsmall ghe, Cyrillicsmall o, Cyrillicsmall er,
      Cyrillicsmall i, Cyrillicsmall a, Cyrillicsmall el, Cyrillicsmall soft sign,
      Cyrillicsmall en, Cyrillicsmall yeru, Cyrillicsmall ie, Cyrillic small es,
      Cyrillicsmall o, Cyrillicsmall es, Cyrillicsmall te, Cyrillicsmall o,
      Cyrillicsmall ya, Cyrillicsmall en, Cyrillicsmall i, Cyrillicsmall ya, Cyrillic
OT  - small es, Cyrillicsmall i, Cyrillicsmall en, Cyrillicsmall ha, Cyrillicsmall er, 
      Cyrillicsmall o, Cyrillicsmall en, Cyrillicsmall i, Cyrillicsmall es,
      Cyrillicsmall te, Cyrillicsmall i, Cyrillicsmall che, Cyrillicsmall en,
      Cyrillicsmall o, Cyrillicsmall es, Cyrillicsmall te, Cyrillicsmall soft sign,
      Cyrillic
CRDT- 2016/01/20 06:00
AID - 10.1111/1468-5922.12196 [doi]
PST - ppublish
SO  - J Anal Psychol. 2016 Feb;61(1):79-85. doi: 10.1111/1468-5922.12196.
</pre>