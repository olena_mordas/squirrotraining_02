<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<pre>
PMID- 26785482
OWN - NLM
STAT- Publisher
DA  - 20160119
LR  - 20160119
IS  - 1095-9203 (Electronic)
IS  - 0036-8075 (Linking)
VI  - 350
IP  - 6265
DP  - 2015 Dec 4
TI  - Overcoming the electroluminescence efficiency limitations of perovskite
      light-emitting diodes.
PG  - 1222-1225
AB  - Organic-inorganic hybrid perovskites are emerging low-cost emitters with very
      high color purity, but their low luminescent efficiency is a critical drawback.
      We boosted the current efficiency (CE) of perovskite light-emitting diodes with a
      simple bilayer structure to 42.9 candela per ampere, similar to the CE of
      phosphorescent organic light-emitting diodes, with two modifications: We
      prevented the formation of metallic lead (Pb) atoms that cause strong exciton
      quenching through a small increase in methylammonium bromide (MABr) molar
      proportion, and we spatially confined the exciton in uniform MAPbBr3 nanograins
      (average diameter = 99.7 nanometers) formed by a nanocrystal pinning process and 
      concomitant reduction of exciton diffusion length to 67 nanometers. These changes
      caused substantial increases in steady-state photoluminescence intensity and
      efficiency of MAPbBr3 nanograin layers.
CI  - Copyright (c) 2015, American Association for the Advancement of Science.
FAU - Cho, Himchan
AU  - Cho H
AD  - Department of Materials Science and Engineering, Pohang University of Science and
      Technology (POSTECH), 77 Cheongam-Ro, Pohang, Gyungbuk 790-784, Republic of
      Korea.
FAU - Jeong, Su-Hun
AU  - Jeong SH
AD  - Department of Materials Science and Engineering, Pohang University of Science and
      Technology (POSTECH), 77 Cheongam-Ro, Pohang, Gyungbuk 790-784, Republic of
      Korea.
FAU - Park, Min-Ho
AU  - Park MH
AD  - Department of Materials Science and Engineering, Pohang University of Science and
      Technology (POSTECH), 77 Cheongam-Ro, Pohang, Gyungbuk 790-784, Republic of
      Korea.
FAU - Kim, Young-Hoon
AU  - Kim YH
AD  - Department of Materials Science and Engineering, Pohang University of Science and
      Technology (POSTECH), 77 Cheongam-Ro, Pohang, Gyungbuk 790-784, Republic of
      Korea.
FAU - Wolf, Christoph
AU  - Wolf C
AD  - Department of Materials Science and Engineering, Pohang University of Science and
      Technology (POSTECH), 77 Cheongam-Ro, Pohang, Gyungbuk 790-784, Republic of
      Korea.
FAU - Lee, Chang-Lyoul
AU  - Lee CL
AD  - Advanced Photonics Research Institute (APRI), Gwangju Institute of Science and
      Technology (GIST), 1 Oryong-dong, Buk-gu, Gwangju 500-712, Republic of Korea.
FAU - Heo, Jin Hyuck
AU  - Heo JH
AD  - Department of Chemical Engineering, College of Engineering, Kyung Hee University,
      1 Seochon-dong, Giheung-gu, Youngin-si, Gyeonggi-do 446-701, Republic of Korea.
FAU - Sadhanala, Aditya
AU  - Sadhanala A
AD  - Cavendish Laboratory, University of Cambridge, J J Thomson Avenue, Cambridge CB3 
      0HE, UK.
FAU - Myoung, NoSoung
AU  - Myoung N
AD  - Advanced Photonics Research Institute (APRI), Gwangju Institute of Science and
      Technology (GIST), 1 Oryong-dong, Buk-gu, Gwangju 500-712, Republic of Korea.
FAU - Yoo, Seunghyup
AU  - Yoo S
AD  - Department of Electrical Engineering, Korea Advanced Institute of Science and
      Technology (KAIST), 373-1 Guseong-dong, Yuseong-gu, Daejeon 305-701, Republic of 
      Korea.
FAU - Im, Sang Hyuk
AU  - Im SH
AD  - Department of Chemical Engineering, College of Engineering, Kyung Hee University,
      1 Seochon-dong, Giheung-gu, Youngin-si, Gyeonggi-do 446-701, Republic of Korea.
FAU - Friend, Richard H
AU  - Friend RH
AD  - Cavendish Laboratory, University of Cambridge, J J Thomson Avenue, Cambridge CB3 
      0HE, UK.
FAU - Lee, Tae-Woo
AU  - Lee TW
AD  - Department of Materials Science and Engineering, Pohang University of Science and
      Technology (POSTECH), 77 Cheongam-Ro, Pohang, Gyungbuk 790-784, Republic of
      Korea. Department of Chemical Engineering, Division of Advanced Materials
      Science, School of Environmental Science and Engineering, Pohang University of
      Science and Technology (POSTECH), 77 Cheongam-Ro, Nam-Gu, Pohang, Gyungbuk
      790-784, Republic of Korea. twlee@postech.ac.kr taewlees@gmail.com.
LA  - ENG
PT  - JOURNAL ARTICLE
DEP - 20151203
TA  - Science
JT  - Science (New York, N.Y.)
JID - 0404511
CRDT- 2016/01/20 06:00
PHST- 2015/08/04 [received]
PHST- 2015/10/22 [accepted]
PHST- 2015/12/03 [aheadofprint]
AID - 350/6265/1222 [pii]
AID - 10.1126/science.aad1818 [doi]
PST - ppublish
SO  - Science. 2015 Dec 4;350(6265):1222-1225. Epub 2015 Dec 3.
</pre>